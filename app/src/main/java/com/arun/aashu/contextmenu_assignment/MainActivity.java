package com.arun.aashu.contextmenu_assignment;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

//    int itemposition;

    ListView listView;
    EditText ed1,e1;
    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listview1);
        ed1 = findViewById(R.id.editText1);


        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);

        listView.setAdapter(adapter);

//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
//
//                itemposition=position;
//                return false;
//            }
//        });

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = ed1.getText().toString();

                arrayList.add(name);

                adapter.notifyDataSetChanged();


            }
        });

        registerForContextMenu(listView);


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add("Rename");
        menu.add("Delete");


        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {



        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int listPosition = info.position;
//        Toast.makeText(this, String.valueOf(listPosition), Toast.LENGTH_SHORT).show();




        switch (item.getTitle().toString()) {

            case "Rename":
                final Dialog dialogText = new Dialog(MainActivity.this);
                dialogText.setContentView(R.layout.text_custom_dialog);
                dialogText.setCancelable(true);
                dialogText.show();
                e1=dialogText.findViewById(R.id.edittext1);
                e1.setText(arrayList.get(listPosition));


                dialogText.findViewById(R.id.renameButton).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String rename = e1.getText().toString();
                        arrayList.set(listPosition, rename);

                        adapter.notifyDataSetChanged();


                        dialogText.dismiss();
                    }
                });
                break;

            case "Delete":

                arrayList.remove(listPosition);
                adapter.notifyDataSetChanged();
                break;
        }


        return super.onContextItemSelected(item);
    }
}
